import json
import os
import pickle
import sys

import websockets

from spe_ed_game.src.game.moves import Move
from src.train.network_inputs import NetworkInputs


class WebSocketClient():

    def __init__(self):
        # load neural network
        if len(sys.argv) != 2:
            print("Wrong parameters!")
            sys.exit(-1)
        try:
            self.net = self.load_object("networks/"+sys.argv[1])
        except:
            print("Wrong parameters!")
            sys.exit(-1)

    def load_object(self, filename):
        # load object with pickle
        with open(filename, 'rb') as f:
            obj = pickle.load(f)
        return obj

    async def connect(self):
        '''
            Connecting to webSocket server

            websockets.client.connect returns a WebSocketClientProtocol, which is used to send and receive messages
        '''
        uri = os.environ['URL'] + "?key=" + os.environ['KEY']
        self.connection = await websockets.client.connect(uri)
        if self.connection.open:
            print('Connection stablished. Client correcly connected')
            return self.connection

    async def sendMessage(self, message):
        '''
            Sending message to webSocket server
        '''
        await self.connection.send(message)

    async def receiveMessage(self, connection):
        '''
            Receiving all server messages and handling them
        '''
        while True:
            try:
                # receive message from server
                message = await connection.recv()
                print("GameState received")
                # calculate network inputs
                inputs = NetworkInputs(message, 5)
                network_input = []
                network_input.append(inputs.ray_front)
                network_input.append(inputs.ray_left)
                network_input.append(inputs.ray_right)
                network_input.append(inputs.speed)

                #get best move from neural network
                output = self.net.activate(network_input)
                choice = max(enumerate(output), key=(lambda x: x[1]))[0]
                move = list(Move)[choice]
                # print and send best move to server
                print("Picking " + move.value)
                await self.sendMessage(json.dumps({"action": move.value}))
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break
