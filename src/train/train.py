from __future__ import print_function

import datetime
import json
import multiprocessing
import os
import pickle
import random
import time
from types import SimpleNamespace

import neat
import numpy as np

from multiprocessing import Process
from spe_ed_game.src.game.directions import Direction
from spe_ed_game.src.game.moves import Move
from spe_ed_game.src.game.spe_ed_game import Game
from spe_ed_game.src.game.player import Player
from src.train.network_inputs import NetworkInputs

counter = 0
# timestamp when training starts
timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")


def save_object(obj, filename):
    # save network with pickle
    with open(filename, 'wb') as output:
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)


def start_thread_game(procnum, return_dict, players, genos, nets, alivePlayers, width, height, guiToggle, FPS,
                      blocksize=10):
    # create a new game
    game = Game(width, height, players, FPS, guiToggle, blocksize, rounds_kill_counts=30)
    round = 0
    while True:
        for num, player in enumerate(players):
            # test if player is still alive
            if not player.alive:
                continue
            raw_json = game.returnJson(player)

            # calculate inputs from json and append them
            inputs = NetworkInputs(raw_json, 4)
            network_input = []
            network_input.append(inputs.ray_front)
            network_input.append(inputs.ray_left)
            network_input.append(inputs.ray_right)
            network_input.append(inputs.speed)
            # get best move from neural network
            output = nets[num].activate(network_input)
            choice = max(enumerate(output), key=(lambda x: x[1]))[0]
            # execute move
            game.makeMove(num, list(Move)[choice])
        # update game board
        game.update()
        round += 1

        # update fitness with the amount of killed players
        for num, player in enumerate(players):
            if game.players[num].alive:
                genos[num].fitness = game.kills_per_player[num]
        if not game.running():
            # end game and return best network
            game.exit()
            print("GAME " + str(procnum) + " ENDS")
            best_id = 0
            best_val = 0
            for num, geno in enumerate(genos):
                if geno.fitness > best_val:
                    best_val = geno.fitness
                    best_id = num
            return_dict[str(procnum)] = [nets[best_id], genos[best_id], players[best_id], genos]
            return [nets[best_id], genos[best_id], players[best_id]]


def eval_genomes(genomes, config):
    Player.count = 1
    nets = []
    players = []
    genos = []
    alivePlayers = []
    width = 70
    height = 70
    # activates tournament mode
    tournament = True
    # number of players per game in tournament mode
    players_ingame = 20
    playerCount = 0
    random.shuffle(genomes)
    for genome_id, genome in genomes:
        playerCount += 1
        genome.fitness = 0
        # create new neural networks with a given config and genome
        nets.append(neat.nn.RecurrentNetwork.create(genome, config))
        genos.append(genome)
        # create new players
        x = random.randint(0, width - 1)
        y = random.randint(0, height - 1)
        dir = random.choice(list(Direction))
        color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        players.append(Player((x, y), dir, color, None))
        alivePlayers.append(True)
        if Player.count > players_ingame and tournament:
            Player.count = 1
    if tournament:
        # TOURNAMENT MODE
        games = []
        i = 0
        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        # create games with given amount of players
        while i <= (playerCount / players_ingame) - 1:
            games.append(Process(target=start_thread_game,
                                 args=(i, return_dict, players[i * players_ingame:(i + 1) * players_ingame],
                                       genos[i * players_ingame:(i + 1) * players_ingame],
                                       nets[i * players_ingame:(i + 1) * players_ingame],
                                       alivePlayers[i * players_ingame:(i + 1) * players_ingame], width,
                                       height, True, 99999)))
            i += 1
        gamesCount = i
        # start games
        for game in games:
            print("Start Game")
            game.start()

        # wait for games to finish
        for game in games:
            game.join()
        players_finale = []
        genos_finale = []
        nets_finale = []
        alive_players_finale = []

        answers = []

        # get best neural networks and update fitness
        for index in range(gamesCount):
            answers.append(return_dict[str(index)])
            for num, genome in enumerate(return_dict[str(index)][3]):
                genos[num + (index * players_ingame)].fitness = genome.fitness

        # sorts the neural networks by their fitness
        answers.sort(key=lambda x: x[1].fitness, reverse=True)
        finalplayercount = len(answers)
        if finalplayercount > players_ingame:
            finalplayercount = players_ingame

        # resets players for the final
        for i in range(finalplayercount):
            players_finale.append(answers[i][2])
            genos_finale.append(answers[i][1])
            nets_finale.append(answers[i][0])
            alive_players_finale.append(True)
            x = random.randint(0, width - 1)
            y = random.randint(0, height - 1)
            dir = random.choice(list(Direction))
            players_finale[i].num = i + 1
            players_finale[i].alive = True
            players_finale[i].dir = dir
            players_finale[i].position = (x, y)
            players_finale[i].speed = 1
            players_finale[i].round = 1

        # start final
        print("FINAL")
        finale = Process(target=start_thread_game,
                         args=(
                             gamesCount + 1, return_dict, players_finale, genos_finale, nets_finale,
                             alive_players_finale,
                             width, height,
                             True, 30))
        finale.start()
        finale.join()
        # get best neural network
        best = return_dict[str(gamesCount + 1)]
        for num, gnome in enumerate(return_dict[str(gamesCount + 1)][3]):
            genos_finale[num].fitness = gnome.fitness
    else:
        # NORMAL MODE
        dict = {}
        # get best neural network
        best = start_thread_game(1, dict, players, genos, nets, alivePlayers, width, height, True, 30000, 10)
    global counter
    global timestamp
    # save best neural network
    save_object(best[0], "networks/" + str(timestamp) + "gen-" + str(counter) + ".network")
    counter += 1


def run(config_file):
    # load configuration
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    # create the population, which is the top-level object for a NEAT run
    p = neat.Population(config)

    # add a stdout reporter to show progress in the terminal
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)

    # run for up to 300000 generations.
    winner = p.run(eval_genomes, 300000)

    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-4')
    p.run(eval_genomes, 10)


if __name__ == '__main__':
    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config')
    run(config_path)
