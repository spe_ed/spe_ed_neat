import json
import os

import matplotlib.pyplot as plt
import numpy as np


class NetworkInputs:
    def __init__(self, message: str, crop_size: int):
        # get game state from message
        parsed_json = json.loads(message)
        board = np.array(parsed_json["cells"])
        # show board if DISPLAY_GAME is true
        if os.getenv("DISPLAY_GAME") == "true":
            plt.clf()
            plt.imshow(board)
            plt.pause(0.000001)
        # get player information
        playerinfo = parsed_json["players"][str(parsed_json["you"])]  # array indexing starts at 1

        x = playerinfo["x"]
        y = playerinfo["y"]
        dir = playerinfo["direction"]
        # pad board with 1
        self.padded_board = np.pad(board, 10, self.pad_with)

        # crop the board around the player head
        self.crop = self.padded_board[y - crop_size + 10:y + crop_size + 1 + 10,
                    x - crop_size + 10:x + crop_size + 1 + 10]

        # rotate the crop according to the player rotation
        ORIENTATIONS = ["up", "right", "down", "left"]
        for index, value in enumerate(ORIENTATIONS):
            if value == dir:
                for _ in range(index):
                    self.crop = np.rot90(self.crop)
                    pass
        # flatten the array
        board_flat = self.crop.flatten().tolist()

        self.board_input = [(x != 0) * 10 for x in board_flat]

        # create ray for every direction except backwards -> always 0
        if dir == "up":
            dir = 0
        elif dir == "right":
            dir = 1
        elif dir == "down":
            dir = 2
        elif dir == "left":
            dir = 3
        self.ray_front = self.rayInDir(x, y, dir, self.padded_board)
        self.ray_left = self.rayInDir(x, y, (dir - 1) % 4, self.padded_board)
        self.ray_right = self.rayInDir(x, y, (dir + 1) % 4, self.padded_board)

        self.speed = playerinfo["speed"]

    def pad_with(self, vector, pad_width, iaxis, kwargs):
        # pad board with given amount of 1
        pad_value = kwargs.get('padder', 1)
        vector[:pad_width[0]] = pad_value
        vector[-pad_width[1]:] = pad_value

    def rayInDir(self, x, y, vecDir, padded_board):
        # creates a ray to a given direction and returns distance to the next obstacle
        directions_vec = [(0, -1), (1, 0), (0, 1), (-1, 0)]
        level = 0
        coordinate = (x + 10, y + 10)
        vector = directions_vec[vecDir]
        while padded_board[coordinate[1] + vector[1]][coordinate[0] + vector[0]] == 0:
            level += 1
            coordinate = (coordinate[0] + vector[0], coordinate[1] + vector[1])
        return level
