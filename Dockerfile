FROM python:3.9
COPY . .
ENV URL "ws://localhost:5000"
ENV KEY "keykey"
ENV NETWORK "20201126-233310gen-20.network"
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python3.9 -m src.main $NETWORK